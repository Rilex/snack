<?php

use League\Plates\Engine;
use App\Container\App;
use App\Http\Middleware\Authenticate;
use Bramus\Router\Router;
use Dotenv\Dotenv;
use Snack\Db;

require_once dirname(dirname(__FILE__)) . '/vendor/autoload.php';
require_once 'helpers.php';

$dotenv = Dotenv::createImmutable(dirname(dirname(__FILE__)))->load();

$app = App::getInstance()
    ->setDb(new Db())
    ->setTemplates(new Engine($_ENV['DIR'] . 'public/views/'))
    ->setRouter(new Router());

/* Register middleware */
$app->registerMiddleware(Authenticate::class);

require_once $_ENV['DIR'] . '/app/router.php';

return $app;
