<?php

$app->router->set404('\App\Http\Controllers\NotFound@index');

$app->router->mount('/api', function () use ($app) {
    $app->router->post('/new', '\App\Http\Controllers\NewPageController@handle');
    $app->router->post('/get-categories', '\App\Http\Controllers\VueComponentsController@getCategories');
    $app->router->post('/get-pages', '\App\Http\Controllers\VueComponentsController@getPages');
    $app->router->post('/get-page', '\App\Http\Controllers\VueComponentsController@getPage');
});

$app->router->get('/', '\App\Http\Controllers\ViewPageController@home');
$app->router->get('/new', '\App\Http\Controllers\ViewPageController@new');

$app->router->get('/[a-z0-9_-]+', function () {
    echo 'CATEGORY';
});

$app->router->get('/[a-z0-9_-]+/[a-z0-9_-]+', '\App\Http\Controllers\ViewPageController@index');
