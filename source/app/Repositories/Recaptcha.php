<?php

namespace App\Repositories;

class Recaptcha
{
    public static function verify()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptchaResponse'])) {
            $recaptchaUrl = 'https://www.google.com/recaptcha/api/siteverify';
            $recaptchaResponse = $_POST['recaptchaResponse'];
            $recaptcha = file_get_contents($recaptchaUrl . '?secret=' . $_ENV['RECAPTCHA_SECRET'] . '&response=' . $recaptchaResponse);
            $recaptcha = json_decode($recaptcha);
            return $recaptcha;
        }
    }
}
