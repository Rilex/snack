<?php

namespace App\Repositories;

use PDO;
use Snack\Db;

class PageRepository
{
    private const PATTERNS = [
        '/\s+/',
        '/_/',
        '/[^a-z0-9_-]+/',
    ];

    private const REPLACEMENTS = [
        '_',
        '-',
        '',
    ];

    /**
     * @param int $id Category ID, 0 for all categories
     */
    public static function getPagesFromCategory(?int $id = null): array
    {
        $where = !$id ? ' > 0' : " = $id";
        return Db::query("SELECT p.slug,p.title,c.slug as category FROM pages p
        LEFT JOIN categories c
        ON p.category = c.id 
        WHERE p.category $where
        ORDER BY p.updated_at asc")
            ->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @todo make slug unique
     */
    public static function createSlug(string $title): string
    {
        return self::replace($title);
    }

    private static function replace(string $string): string
    {
        $string = strtolower($string);
        $string = preg_replace(self::PATTERNS, self::REPLACEMENTS, $string);
        return $string;
    }
}
