<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository
{
    public static function getCategories(): array
    {
        return (new Category())
            ->orderBy('slug')
            ->all()
            ->get('slug, title');
    }
}
