<?php

namespace App\Models;

use Snack\Model;

final class Category extends Model
{
    protected $table = 'categories';
    protected $id = 'id';
    protected $fillable = ['slug', 'title'];
}
