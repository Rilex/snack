<?php

namespace App\Models;

use Snack\Model;

final class User extends Model
{
    protected $table = 'users';
    protected $id = 'id';
    protected $fillable  = ['username', 'password'];
}
