<?php

namespace App\Models;

use Snack\Model;

final class Page extends Model
{
    protected $table = 'pages';
    protected $id = 'id';
    protected $fillable  = ['slug', 'title', 'content', 'category'];
}
