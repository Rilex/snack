<?php

if (!function_exists('dd') && function_exists('d')) {
    function dd($param)
    {
        d($param);
        die();
    }
}

if (!function_exists('je')) {
    function je(array $array)
    {
        header('Content-Type: application/json');
        echo json_encode($array);
        die();
    }
}

if (!class_exists('Crypt')) {
    class Crypt
    {
        const METHOD = 'BF-CFB';
        public static function encrypt($message): string
        {
            $nonceSize = openssl_cipher_iv_length(self::METHOD);
            $nonce = openssl_random_pseudo_bytes($nonceSize);
            $ciphertext = openssl_encrypt(
                $message,
                self::METHOD,
                self::key(),
                OPENSSL_RAW_DATA,
                $nonce
            );
            return base64_encode($nonce . $ciphertext);
        }

        public static function decrypt($message): string
        {
            error_reporting(~E_WARNING & ~E_NOTICE);
            $message = base64_decode($message, true);
            $nonceSize = openssl_cipher_iv_length(self::METHOD);
            $nonce = mb_substr($message, 0, $nonceSize, '8bit');
            $ciphertext = mb_substr($message, $nonceSize, null, '8bit');
            try {
                $plaintext = openssl_decrypt(
                    $ciphertext,
                    self::METHOD,
                    self::key(),
                    OPENSSL_RAW_DATA,
                    $nonce
                );
                return $plaintext;
            } catch (\Throwable $th) {
                throw new Exception("Error Processing Request", 1);
            }
        }

        private static function key()
        {
            return $_ENV['APP_SECRET'];
        }
    }
}
