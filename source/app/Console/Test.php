<?php

namespace App\Console;

use Snack\Console\Extended\Console;
use Snack\Validator\Validate;

class Test extends Console
{
    public function __construct(array $args)
    {
        $this->args = $args;
        return $this->job();
    }

    private function job(): void
    {
        $err = Validate::field('email-field')
            ->value('aaaa1$')
            ->rule('range')
            ->rule('alpha_numeric')
            ->validate();

        $mem = (int) memory_get_peak_usage() / 1000000;

        $this->setColor('blue')
            ->displayMessage('HELLO WORLD! ' . number_format($mem, 2));
    }
}
