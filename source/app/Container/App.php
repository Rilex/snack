<?php

namespace App\Container;

/**
 * Global container
 */
class App
{
    /**
     * @var self $app
     */
    private static $app = null;

    /** 
     * @var \Bramus\Router\Router
     */
    public $router;

    /**
     * @var \Snack\DbInterface $db
     */
    public $db;

    /**
     * @var \League\Plates\Engine $templates
     */
    public $templates;

    /**
     * @return App\Container\App
     */
    public static function getInstance(): self
    {
        if (self::$app == null) {
            self::$app = new App();
        }
        return self::$app;
    }

    public function run()
    {
        $this->router->run();
    }

    public function setDb(\Snack\DbInterface $db)
    {
        if (!$this->db) {
            $this->db = $db::connection() ?? die('Database connection failed.');
        }
        return $this;
    }

    public function setTemplates(\League\Plates\Engine $templates)
    {
        $this->templates = $templates;
        return $this;
    }

    public function setRouter(\Bramus\Router\Router $router): App
    {
        $this->router = $router;
        return $this;
    }

    /**
     * @param string $middleware
     */
    public function registerMiddleware($middleware)
    {
        (new $middleware())->handle();
    }

    protected function __clone()
    {
    }

    protected function __construct()
    {
    }
}
