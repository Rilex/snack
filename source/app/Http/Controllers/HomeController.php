<?php

namespace App\Http\Controllers;

use App\Container\App;

/**
 * @property \App\Container\App
 */
final class HomeController
{
    public function index()
    {
        $app = App::getInstance();
        echo $app->templates->render('pages/page');
    }
}
