<?php

namespace App\Http\Controllers;

use App\Container\App;
use Snack\Http\Controller;

/**
 * @property \App\Container\App $app
 */
final class NotFound
{
    public function index()
    {
        $app = App::getInstance();

        echo $app->templates->render('pages/404');
    }
}
