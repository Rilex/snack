<?php

namespace App\Http\Controllers;

use App\Container\App;
use App\Models\Category;
use App\Models\Page;
use App\Repositories\PageRepository;

/**
 * @property \App\Container\App $app
 */
final class NewPageController
{
    private const CODES = [
        422 => 'Unprocessable Entity'
    ];

    public function __construct()
    {
        header('Content-Type: application/json; charset=UTF-8');
        $this->data = json_decode(file_get_contents("php://input"));
    }

    public function index()
    {
        /**
         * validate if set and not empty
         */
        if (
            isset($this->data->title) && $this->data->title &&
            isset($this->data->content) && $this->data->content &&
            isset($this->data->category) && $this->data->category
        ) {

            $data = [
                'slug' => PageRepository::createSlug($this->data->title),
                'title' => $this->data->title,
                'content' => $this->data->content,
                'category' => (new Category())
                    ->where("slug", '=', $this->data->category)
                    ->get('id')
            ];

            if ((new Page())->save($data)) {

                echo json_encode($data);
            } else {

                http_response_code(422);
                echo json_encode(
                    ['message' => self::CODES[422]]
                );
            }
        } else {

            http_response_code(422);
            echo json_encode(
                ['message' => self::CODES[422]]
            );
        }
    }
}
