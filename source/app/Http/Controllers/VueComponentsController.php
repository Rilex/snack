<?php

namespace App\Http\Controllers;

use App\Container\App;
use App\Http\Resources\Categories;
use App\Http\Resources\Page;
use App\Http\Resources\Pages;
use App\Models\Category;
use App\Models\Page as PageModel;
use App\Repositories\CategoryRepository;
use App\Repositories\PageRepository;

final class VueComponentsController
{
    /**
     * @var \App\Container\App $app
     */
    private $app;

    /**
     * @var \stdClass $data Request data
     */
    private $data;

    public function __construct()
    {
        // $this->app = App::getInstance();
        header('Content-Type: application/json; charset=UTF-8');
        $this->data = json_decode(file_get_contents("php://input"));
    }

    public function getcategories()
    {
        echo json_encode((new Categories(CategoryRepository::getCategories()))->toArray());
    }

    public function getPages()
    {
        if (isset($this->data->slug)) {

            $categoryId = (new Category)
                ->where('slug', '=', $this->data->slug)
                ->get('id');

            if (count($categoryId)) {

                echo json_encode((new Pages(PageRepository::getPagesFromCategory($categoryId[0])))->toArray());
            } else {

                /* Page not found */
                http_response_code(404);
                echo json_encode(
                    ['message' => 'Category not found.']
                );
            }
        } else {

            /* Missing parameters */
            http_response_code(422);
            echo json_encode(
                ['message' => 'Missing parameter \'slug\'']
            );
        }
    }

    public function getPage()
    {
        if (isset($this->data->slug)) {

            $page = (new PageModel)
                ->where('slug', '=', $this->data->slug)
                ->get();

            if (isset($page['slug'])) {

                echo json_encode((new Page($page))->toArray());
            } else {

                /* Page not found */
                http_response_code(404);
                echo json_encode(
                    ['message' => 'Page not found.']
                );
            }
        } else {

            /* Missing parameters */
            http_response_code(422);
            echo json_encode(
                ['message' => 'Missing parameter \'slug\'']
            );
        }
    }
}
