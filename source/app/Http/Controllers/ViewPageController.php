<?php

namespace App\Http\Controllers;

use App\Container\App;
use App\Http\Resources\Categories;
use App\Http\Resources\Page;
use App\Models\Category;
use App\Models\Page as PageModel;
use App\Repositories\CategoryRepository;

/**
 * @property \App\Container\App
 */
final class ViewPageController
{
    /**
     * @var \App\Container\App $app
     */
    private $app;

    public function __construct()
    {
        $this->app = App::getInstance();
    }

    public function index()
    {
        $req = (parse_url($_SERVER["REQUEST_URI"]));
        $cat_slug = explode('/', $req['path'])[1];
        $page_slug = basename($req['path']);

        $categoryId = (new Category)
            ->where('slug', '=', $cat_slug)
            ->get('id')[0];

        if ($categoryId) {

            $page = (new PageModel())
                ->where('slug', '=', $page_slug)
                ->where('category', '=', $categoryId)
                ->get();

            if (count($page)) {
                $this->app->templates->addData(['page' => [$page_slug, $cat_slug]]);
                echo $this->app->templates->render('pages/page');
                return;
            }
        }

        echo $this->app->templates->render('pages/404');
    }

    public function home()
    {
        echo $this->app->templates->render('pages/page');
    }

    public function new()
    {
        echo $this->app->templates->render('pages/editor');
    }

    private static function getCategories(): array
    {
        return (new Categories(CategoryRepository::getCategories()))->toArray();
    }
}
