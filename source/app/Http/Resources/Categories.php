<?php

namespace App\Http\Resources;

final class Categories
{

    public function __construct(array $resource)
    {
        $this->resource = $resource;
    }

    public function toArray(): array
    {
        return ['categories' => $this->resource];
    }
}
