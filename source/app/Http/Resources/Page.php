<?php

namespace App\Http\Resources;

use App\Models\Category;

final class Page
{
    /**
     * Escape characters to html
     * <?,
     */
    private const REGEX = [
        'pattern' => ['/<\?/'],
        'replace' => ['&lt;?']
    ];

    /**
     * @var array $resource
     */
    private $resource;

    public function __construct(array $resource)
    {
        $this->resource = $resource;
    }

    public function toArray(): array
    {
        $array = &$this->resource;

        return ['page' => [
            'slug' => $array['slug'],
            'title' => $array['title'],
            'content' => self::filter($array['content']),
            'category' => (new Category())->find($array['category']),
            'created_at' => $array['created_at'],
        ]];
    }

    private static function filter(string $string): string
    {
        return preg_replace((self::REGEX['pattern']), self::REGEX['replace'], $string);
    }
}
