<?php

namespace App\Http\Resources;

final class Pages
{

    public function __construct(array $resource)
    {
        $this->resource = $resource;
    }

    public function toArray(): array
    {
        return ['pages' => $this->resource];
    }
}
