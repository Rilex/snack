<?php

namespace Snack;

use App\Container\App;
use PDO;

abstract class Model
{
    const FETCH_MODE = [
        'fetch',
        'fetchAll',
    ];

    /** @var \PDO $db */
    private $db;

    /** @var \PDOStatement $stmt */
    private $stmt;

    private $distinct = '*';
    private $query = '';
    private $whereClause = [];
    private $orderByClause = '';
    private $limit = -1;
    private $fetchMode = '';
    private $result = [];

    public function __construct()
    {
        $this->db = App::getInstance()->db;
    }

    /**
     * @param int|string $value
     */
    public function where(string $column, string $operator, string $value): self
    {
        $this->whereClause[] = [$column, $operator, $value];
        return $this;
    }

    public function orderBy(string $column): self
    {
        $this->orderByClause = $column;
        return $this;
    }

    public function first(): array
    {
        $this->limit = 2;
        $this->buildSelectQuery();
        $this->result = $this->stmt->fetch(PDO::FETCH_ASSOC);
        return $this->result ?: [];
    }

    public function all(): self
    {
        $this->fetchMode = self::FETCH_MODE[1];
        return $this;
    }

    public function get(string $distinct  = null): array
    {
        if ($distinct) {
            $this->distinct = $distinct;
        }

        $this->buildSelectQuery();

        if ($this->fetchMode == self::FETCH_MODE[1]) {
            $this->result = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $this->result = $this->stmt->fetch(PDO::FETCH_ASSOC);
        }

        if ($distinct && is_array($this->result)) {
            return array_values($this->result);
        } else {
            return $this->result ?: [];
        }
    }

    /**
     * @param return array:bool
     */
    public function find(int $id): array
    {
        $this->query = "SELECT * FROM $this->table WHERE id = :id";
        $this->stmt = $this->db->prepare($this->query);
        $this->stmt->execute(['id' => $id]);
        $this->result = $this->stmt->fetch(PDO::FETCH_ASSOC);
        return $this->result ?: [];
    }

    public function save(array $data): bool
    {
        $fields = implode(',', array_keys($data));
        $this->query = "INSERT INTO $this->table ($fields) VALUES (:";
        $this->query .= implode(',:', array_keys($data)) . ')';
        return $this->db->prepare($this->query)->execute($data);
    }

    public function saveMany(array $columns, array $data): bool
    {
        $fields = implode(',', $columns);
        $this->query = "INSERT INTO $this->table ($fields) VALUES (:";
        $this->query .= implode(',:', $columns) . ')';
        $this->stmt = $this->db->prepare($this->query);
        $bool = true;
        foreach ($data as $value) {
            $bool = $this->stmt->execute(array_combine($columns, $value));
        }
        return $bool;
    }

    private function buildSelectQuery(): self
    {
        /* SELECT */
        $this->query = "SELECT $this->distinct FROM $this->table";

        /* WHERE */
        if (count($this->whereClause)) {
            $this->query .= ' WHERE';
            foreach ($this->whereClause as $key => $value) {
                $this->query .= (($key > 0) ? ' AND' : '') . " $value[0] $value[1] :$value[0]";
            }
            $array = [];
            foreach ($this->whereClause as $value) {
                $array[$value[0]] = $value[2];
            }
        }

        /* ORDER BY*/
        if ($this->orderByClause) {
            $this->query .= " ORDER BY $this->orderByClause";
        }

        /* LIMIT*/
        if ($this->limit > 0) {
            $this->query .= " LIMIT $this->limit";
        }

        $this->stmt = $this->db->prepare($this->query);
        $this->stmt->execute(isset($array) ? $array : []);

        return $this;
    }
}
