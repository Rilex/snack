<?php

namespace Snack;

interface DbInterface
{
    public static function connection();
    public static function query(string $query);
}
