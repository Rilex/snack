<?php

namespace Snack\Validator;

use Snack\Exception\ValidateException;

final class Validate
{
    public $rules = [
        'range' => '/^.{4,26}$/',
        'numeric' => '/^[0-9]+$/',
        'alpha' => '/^[a-zA-z]+$/',
        'alpha_numeric' => '/^[a-zA-z0-9]+$/',
        'email' => '/^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/'
    ];

    private $messages = [
        'generic' => 'This field is invalid.',
        'ranges' => 'Please provide at least 4 characters',
        'numeric' => 'Only numbers.',
        'alpha' => 'Only letters.',
        'alpha_numeric' => 'Only numbers and letters.',
        'email' => 'Email is invalid format.'
    ];

    private $field = '';
    private $value = '';
    private $errors = [];

    public static function field($field)
    {
        return new self($field);
    }

    public function value($value)
    {
        $this->value = $value;
        return $this;
    }

    public function rule($rule)
    {
        if (!isset($this->rules[$rule])) {
            throw new ValidateException("The rule '$rule' doesn't exist");
        }

        $message = isset($this->messages[$rule]) ?
            $this->messages[$rule] : $this->messages['generic'];

        if (!preg_match($this->rules[$rule], $this->value, $match)) {
            $this->errors[$this->field][] = $message;
        }
        return  $this;
    }

    public function validate(): array
    {
        return $this->errors;
    }

    private function __construct($field)
    {
        $this->field = $field;
    }
}
