<?php

namespace Snack\Console\Extended;

abstract class Console
{
    protected $ags;

    /**
     * @var string $color default gray color
     */
    private $color = '39';

    protected const COLORS = [
        'white' => '97',
        'red' => '31',
        'blue' => '34',
        'green' => '32',
        'default' => '39'
    ];

    protected function setColor(string $color)
    {
        $this->color = self::COLORS[$color];
        return $this;
    }

    protected function displayMessage(string $message)
    {
        print "\e[" . $this->color . "m$message\e[" . self::COLORS['default'] . "m\n";
        return $this;
    }

    protected function clear()
    {
        print "\e[H\e[J";
        return $this;
    }
}
