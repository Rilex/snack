<?php

namespace Snack\Console;

use Snack\Console\Extended\Console;

class Seeder extends Console
{
    public function __construct(array $args)
    {
        $this->args = $args;
        return $this->job();
    }

    private function job(): void
    {
        $this->clear()
            ->setColor('blue')
            ->displayMessage('Seeding database...');

        try {
            $array = json_decode(
                file_get_contents($_ENV['DIR'] . 'database/factories/' . $this->args[2] . '.json'),
                true
            );

            $class = 'App\\Models\\' . $array['model'];
            $model = new $class();

            if (!true == $model->saveMany($array['columns'], $array['data'])) {
                $this->setColor('red')
                    ->displayMessage('There were some MySQL errors while seeding database');
            } else {
                $this->setColor('green')
                    ->displayMessage('Database seeding is complete.');
            }
        } catch (\Throwable $th) {
            $this->setColor('red')
                ->displayMessage('Database seeding failed with error:')
                ->displayMessage($th);;
        }
    }
}
