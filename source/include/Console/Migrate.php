<?php

namespace Snack\Console;

use Snack\Console\Extended\Console;
use Snack\Db;

/**
 * EXAMPLE: php snack migrate
 * @property $args
 */
class Migrate extends Console
{
    public function __construct(array $args)
    {
        $this->args = $args;
        return $this->job();
    }

    /**
     * @todo Migration error checking only works for the very fist query;
     */
    private function job(): void
    {
        try {
            $this->clear()
                ->setColor('blue')
                ->displayMessage('Migrating database...');

            Db::query(file_get_contents($_ENV['DIR'] . 'database/migrations/migration.sql'));

            $this->setColor('green')
                ->displayMessage('Database migration is complete.');
        } catch (\Throwable $th) {

            $this->setColor('red')
                ->displayMessage('Database migration failed:')
                ->displayMessage($th);
        }
    }
}
