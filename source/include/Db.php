<?php

namespace Snack;

use App\Container\App;
use Exception;

final class Db implements DbInterface
{
    public static function connection(): ?\PDO
    {
        try {
            return new \PDO(
                "mysql:host=$_ENV[DB_HOST];dbname=$_ENV[DB_DATABASE]",
                $_ENV['DB_USERNAME'],
                $_ENV['DB_PASSWORD']
            );
        } catch (\Throwable $th) {
            return null;
        }
    }

    public static function query(string $query): \PDOStatement
    {
        /**
         * @var \Pdo $db
         */
        $db = App::getInstance()->db;

        try {
            return $db->query($query);
        } catch (\Throwable $th) {
            throw new Exception($db->errorInfo()[2]);
        }
    }
}
