<?php
header("HTTP/1.0 404 Not Found");
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    echo json_encode([
        'error' => 'Invalid endpoint.',
        'code' => 404
    ]);
    exit();
}
$this->layout('templates/template', $this->data);
?>

<div class="row mb-5">
    <div class="col-lg-12 text-center">
        <h1 class="mt-4">404 NOT FOUND</h1>
    </div>
</div>