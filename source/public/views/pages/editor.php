<?php
$this->layout('templates/template', $this->data);
?>

<div class="row mb-5">

    <!-- Categories -->
    <div class="col-md-2 nav-bar" id="categories">

        <h5>{{title}}</h5>
        <p v-for="(item, index) in content">
            <a :href="URL +'/'+item.slug" @click.prevent="getPages(item.slug)" :class="{active:item.slug==active}">{{item.title}}</a>
        </p>

    </div>
    <!-- Pages -->
    <div class="col-md-2 nav-bar" id="pages">

        <h5>{{title}}</h5>
        <p v-for="(item, index) in pages">
            <a :href="URL +'/'+item.category+'/'+item.slug" @click.prevent="getPage(item.slug,true)" :class="{active:item.slug==active}">{{item.title}}</a>
        </p>

    </div>
    <!-- Editor -->
    <div class="col-7" id="content">
        <template v-if="rendered">

            <div class="col-lg-12 text-center">
                <h1>{{title}}</h1>
            </div>

            <form @submit.prevent="formSubmit" action="#" method="POST">

                <template>
                    <div v-if="form.message" class="alert alert-primary" role="alert">
                        {{form.message}}
                    </div>
                </template>

                <div class="form-group">
                    <input type="text" placeholder="Title" autocomplete="off">
                </div>

                <div class="form-group row">
                    <div class="col-sm-2">Category:</div>
                    <div class="col-sm-10">
                        <select>
                            <option v-for="(item, index) in form.categories">{{item.title}}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <textarea cols="30" rows="24" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </form>
        </template>
    </div>
</div>

<script>
    "use strict";
    const URL = "<?= $_ENV['URL'] ?>";
    const HEADERS = {
        'content-type': 'application/json',
    };

    var content = new Vue({
        el: '#content',
        data: {
            title: "Create New Page",
            rendered: null,
            form: {
                title: "some title",
                categories: [],
                message: null,
            },
        },
        mounted: function() {
            this.getCategories();
        },
        methods: {

            async getCategories() {
                const response = await fetch('/api/get-categories', {
                    method: 'POST',
                    headers: HEADERS,
                });
                const result = await response.json();
                if (response.ok) {
                    this.form.categories = result.categories;
                    this.rendered = true;
                }
            },



            async formSubmit() {


                const response = await fetch('/api/new', {
                    method: 'POST',
                    headers: {
                        'content-type': 'application/json',
                    },
                    body: JSON.stringify(this.form)
                });

                const result = await response.json();

                if (response.ok) {
                    console.log(result);
                    this.message = result.message;
                    this.alert = true;
                }
            },
        },
    })
</script>