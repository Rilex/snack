<?php
$this->layout('templates/template');
list($page, $category) = isset($page) ? $page : [null, null];
?>

<div class="row mb-5">
    <!-- Categories -->
    <div class="col-md-2 nav-bar bg-primary" id="categories">
        <template v-if="content">
            <h5 class="m-2">{{title}}</h5>
            <p v-for="(item, index) in content">
                <a :href="URL +'/'+item.slug" @click.prevent="getPages(item.slug)" :class="{active:item.slug==active}">{{item.title}}</a>
            </p>
        </template>
    </div>
    <!-- Pages -->
    <div class="col-md-2 nav-bar" id="pages">
        <template v-if="title">
            <h5 class="m-2">{{title}}</h5>
            <p v-for="(item, index) in pages">
                <a :href="URL +'/'+item.category+'/'+item.slug" @click.prevent="getPage(item.slug,true)" :class="{active:item.slug==active}">{{item.title}}</a>
            </p>
        </template>
    </div>
    <!-- Page -->
    <div class="col-md-7" id="page">
        <template v-if="title">
            <div class="col-lg-12 text-center">
                <h1>{{title}}</h1>
            </div>
            <div class="col-lg-12" v-html="content">
            </div>
        </template>
    </div>
</div>

<script>
    "use strict";
    const URL = "<?= $_ENV['URL'] ?>";
    const HEADERS = {
        'content-type': 'application/json',
    };
    const PAGE = "<?= $page ?>";
    const CATEGORY = "<?= $category ?>";

    /* Categories Component */
    let categories = new Vue({
        el: "#categories",
        data: {
            title: "Categories",
            content: [],
            active: ""
        },
        mounted: function() {
            this.getCategories();
            this.getPages(CATEGORY);
        },
        methods: {
            async getCategories() {
                const response = await fetch('/api/get-categories', {
                    method: 'POST',
                    headers: HEADERS,
                });
                const result = await response.json();
                if (response.ok) {
                    this.content = result.categories;
                }
            },
            async getPages(slug) {
                const response = await fetch('/api/get-pages', {
                    method: 'POST',
                    headers: HEADERS,
                    body: JSON.stringify({
                        slug: slug
                    })
                });
                const result = await response.json();
                if (response.ok) {
                    categories.active = slug;
                    pages.pages = result.pages;
                }
            }
        }
    });

    /* Pages Component */
    let pages = new Vue({
        el: "#pages",
        data: {
            title: "Pages",
            pages: [],
            active: PAGE
        },
        mounted: function() {
            this.getPage(PAGE, false);
        },
        methods: {
            async getPage(slug, fromMounted) {
                const response = await fetch('/api/get-page', {
                    method: 'POST',
                    headers: HEADERS,
                    body: JSON.stringify({
                        slug: slug
                    })
                });
                const result = await response.json();
                if (response.ok) {
                    categories.active = result.page.category.slug;
                    pages.active = result.page.slug;
                    page.content = result.page.content;
                    page.title = result.page.title;

                    this.$nextTick(() => {
                        page.highlightPost();
                    });

                    if (fromMounted) {
                        window.history.pushState({
                                page: result.page.slug,
                                category: result.page.category.slug
                            }, null,
                            URL + "/" + result.page.category.slug + "/" + result.page.slug);
                    }
                }
            },
        }
    });

    /* Pages Component */
    let page = new Vue({
        el: "#page",
        data: {
            title: "",
            content: "",
        },
        methods: {
            highlightPost(event) {
                document.getElementById("page").querySelectorAll('code').forEach((block) => {
                    hljs.highlightBlock(block)
                })
            }
        }
    });

    window.addEventListener('popstate', function(e) {
        if (e.state) {
            categories.getPages(e.state.category);
            pages.getPage(e.state.page, false);
        } else {
            categories.getPages(CATEGORY);
            pages.getPage(PAGE, false);
        }

    });
</script>