<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $_ENV['DESCRIPTION'] ?>">
    <meta name="author" content="">
    <title><?= $_ENV['TITLE'] . " - " . $_ENV['DESCRIPTION'] ?></title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.3/vue.min.js"></script>
    <link rel="stylesheet" href="/dist/css/app.css">
    <link rel="stylesheet" href="/dist/css/ayu-highlight.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.0.3/highlight.min.js"></script>
</head>

<body>

    <?php $this->insert('partials/header') ?>

    <div class="container-fluid mt-5">
        <?= $this->section('content') ?>
    </div>
    <script src="/dist/js/app.js"></script>
    <?//<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap.native/3.0.0/bootstrap-native.min.js"></script>?>
</body>

</html>