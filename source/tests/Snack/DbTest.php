<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Snack\Db;

final class DbTest extends TestCase
{
    public function testDbConnection()
    {
        $this->assertTrue((Db::connection() instanceof \PDO));
    }

    public function testDbConnectionFail()
    {
        unset($_ENV);
        $this->assertTrue((Db::connection() == null));
    }

    public function testQuery()
    {
        $this->assertTrue(Db::query('SELECT * FROM users') instanceof \PDOStatement);
    }

    public function testError()
    {
        $this->expectException('\Throwable');
        Db::query('SELECT * FROM ');
    }
}
