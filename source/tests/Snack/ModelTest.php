<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Snack\Db;

final class ModelTest extends TestCase
{
    public function testInstance()
    {
        $this->assertTrue((new \App\Models\User()) instanceof \Snack\Model);
    }
}
