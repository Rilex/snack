.PHONY: dev

net: create_newtork
dev: fix_files_owners_and_permissions
install: install_wordpress

create_newtork:
	@echo "Creating Docker network..."
	docker network inspect enode5 >/dev/null 2>&1 || \
		docker network create --driver=bridge --subnet=172.18.0.0/16 --gateway=172.18.0.1 enode5
	@echo "Creating docker network - OK"

fix_files_owners_and_permissions:
	@echo "Fixing files owners and permissions..."
	docker-compose exec php-fpm chown -R www-data:www-data /var/www/html
	docker-compose exec php-fpm find . -type d -exec chmod 775 {} \;
	docker-compose exec php-fpm find . -type f -exec chmod 664 {} \;
	@echo "Fixing files owners and permissions - OK"

db_import:
	@echo "Importing database..."
	docker-compose exec mysql mysqldump -utest -ptest --add-drop-table --no-data app | grep ^DROP | \
	docker-compose exec -T mysql mysql -utest -ptest app
	docker-compose exec -T mysql mysql -utest -ptest app < docker/mysql/sqldump/tampocom_wp.sql
	@echo "Importing database - OK"

db_export:
	docker-compose exec mysql mysqldump -utest -ptest app > docker/mysql/sqldump/app.sql

# make composer arg="install --no-dev"
composer:
	docker-compose exec php-fpm composer $(arg)

test:
	clear
	@echo "Testing..."
	docker-compose exec php-fpm vendor/bin/phpunit --verbose
	@echo "Testing - OK"

coverage:
	clear
	@echo "Testing..."
	docker-compose exec php-fpm vendor/bin/phpunit --coverage-html cc
	@echo "Testing - OK"
